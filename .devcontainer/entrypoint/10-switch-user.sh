#!/bin/sh

env | sort

echo "#############################"
echo "EM_USERNAME: ${EM_USERNAME}"
echo "EM_USER_UID: ${EM_USER_UID}"
echo "EM_USER_GID: ${EM_USER_GID}"
echo "id -u : $(id -u)"
echo "EM_ID_FOLDER: ${EM_ID_FOLDER}"
set -x

if [ "$(id -u)" = "0" ]; then
  echo "is root"
  if [ -d "${EM_ID_FOLDER}" ]; then
    echo "folder  ${EM_ID_FOLDER} exists"
    fix-perms -r -u ${EM_USERNAME} -g ${EM_USERNAME} ${EM_ID_FOLDER}
  fi
fi